package com.ead.authuser.services.impl;

import com.ead.authuser.assembler.UserEntityAssembler;
import com.ead.authuser.assembler.UserModelAssembler;
import com.ead.authuser.model.UserModel;
import com.ead.authuser.entity.UserEntity;
import com.ead.authuser.exceptions.UserNotFoundException;
import com.ead.authuser.repositories.UserRepository;
import com.ead.authuser.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserModelAssembler modelAssembler;

    private final UserEntityAssembler entityAssembler;

    private final UserRepository userRepository;




    @Override
    public List<UserEntity> findAll() {
        return userRepository.findAll();
    }

    @Override
    public Optional<UserEntity> findById(UUID userId) {
        return userRepository.findById(userId);
    }

    @Override
    public void delete(UserEntity userEntity) {
        userRepository.delete((userEntity));
    }

    @Override
    public void save(UserEntity userEntity) {
        userRepository.save(userEntity);
    }

    @Override
    public boolean existByUserName(String username) {
        return userRepository.existsByUserName(username);
    }

    @Override
    public boolean existByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public ResponseEntity<Object> updateUser(UUID userId, UserModel userModel) {
   //     var userEntity = findUserIfExist(userId);
        return null;
    }

    @Override
    public UserModel findUser(UUID userId) {
        return modelAssembler.toModel(findUserIfExist(userId));
    }

    private UserEntity findUserIfExist(UUID userId) {
        return userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException("AAAAAAAA"));
    }

}