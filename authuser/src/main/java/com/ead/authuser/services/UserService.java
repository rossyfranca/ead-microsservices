package com.ead.authuser.services;

import com.ead.authuser.model.UserModel;
import com.ead.authuser.entity.UserEntity;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserService {
    List<UserEntity> findAll();

    Optional<UserEntity> findById(UUID userId);

    void delete(UserEntity userEntity);

    void save(UserEntity userEntity);

    boolean existByUserName(String userName);

    boolean existByEmail(String email);

    ResponseEntity<Object> updateUser(UUID userId, UserModel userModel);

    UserModel findUser(UUID userId);
}
