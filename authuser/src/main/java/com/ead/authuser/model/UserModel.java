package com.ead.authuser.model;

import com.ead.authuser.controllers.view.UserEntryView;
import com.ead.authuser.controllers.view.UserReturnView;
import com.ead.authuser.enums.UserStatus;
import com.ead.authuser.enums.UserType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserModel extends RepresentationModel<UserModel> {
    @JsonView({UserReturnView.Default.class})
    private UUID userId;

    @NotNull
    @JsonView(UserEntryView.RegisterUser.class)
    private String nickName;

    @NotNull
    @JsonView(UserEntryView.RegisterUser.class)
    private String email;

    @Size(min = 6, max = 20, groups = { UserEntryView.RegisterUser.class, UserEntryView.UpdatePassword.class })
    @NotBlank(groups = { UserEntryView.RegisterUser.class, UserEntryView.UpdatePassword.class })
    @JsonView({UserEntryView.RegisterUser.class, UserEntryView.UpdatePassword.class})
    private String password;

    @Size(min = 6, max = 20, groups = UserEntryView.UpdatePassword.class)
    @NotBlank(groups = UserEntryView.UpdatePassword.class)
    @JsonView(UserEntryView.UpdatePassword.class)
    private String oldPassword;

    @JsonView({UserEntryView.RegisterUser.class, UserEntryView.UpdateUser.class})
    private String fullName;

    @JsonView({UserEntryView.RegisterUser.class, UserEntryView.UpdateUser.class})
    private String phoneNumber;

    @JsonView({UserEntryView.RegisterUser.class, UserEntryView.UpdateUser.class})
    private String cpf;

    @JsonView({UserEntryView.RegisterUser.class, UserEntryView.UpdateImage.class})
    private String imgUrl;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @JsonView(UserReturnView.Default.class)
    private LocalDateTime creationDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @JsonView(UserReturnView.Default.class)
    private LocalDateTime lastUpdateDate;

    @JsonView({ UserReturnView.Default.class, UserEntryView.FilterUser.class })
    private UserStatus userStatus;

    @JsonView({ UserReturnView.Default.class, UserEntryView.FilterUser.class })
    private UserType userType;
}
