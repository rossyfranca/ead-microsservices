package com.ead.authuser.controllers;

import com.ead.authuser.controllers.view.UserEntryView;
import com.ead.authuser.controllers.view.UserReturnView;
import com.ead.authuser.model.UserModel;
import com.ead.authuser.entity.UserEntity;
import com.ead.authuser.services.UserService;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping
    public ResponseEntity<List<UserEntity>> getAllUsers() {
        return ResponseEntity.status(HttpStatus.OK).body(userService.findAll());
    }

    @JsonView(UserReturnView.Default.class)
    @GetMapping("/{userId}")
    public UserModel getOneUser(@PathVariable(value = "userId") UUID userId) {
        return userService.findUser(userId);
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<Object> deleteUser(@PathVariable(value = "userId") UUID userId) {
        Optional<UserEntity> userModelOptional = userService.findById(userId);

        if (!userModelOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
        } else {
            userService.delete(userModelOptional.get());
            return ResponseEntity.status(HttpStatus.OK).body("User deleted successfully");
        }

    }

    @JsonView(UserReturnView.Default.class)
    @PutMapping("/{userId}")
    public ResponseEntity<Object> updateUser(
            @JsonView(UserEntryView.UpdateUser.class)
            @PathVariable(value = "userId")
            UUID userId,
            UserModel userModel) {

        return userService.updateUser(userId, userModel);

    }
}
