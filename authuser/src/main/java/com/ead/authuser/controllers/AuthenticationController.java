package com.ead.authuser.controllers;

import com.ead.authuser.controllers.view.UserEntryView;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ead.authuser.model.UserModel;
import com.ead.authuser.entity.UserEntity;
import com.ead.authuser.enums.UserStatus;
import com.ead.authuser.enums.UserType;
import com.ead.authuser.services.UserService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(path = "/auth")
@RestController
public class AuthenticationController {

    @Autowired
    UserService userService;

    @PostMapping("/signup")
    public ResponseEntity<Object> registerUser(@Validated @JsonView(UserEntryView.RegisterUser.class) @RequestBody UserModel userModel){
        if(userService.existByUserName(userModel.getNickName())){
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Error: Username is Already taken!");
        }
        if(userService.existByEmail(userModel.getEmail())){
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Error: Email is Already taken!");
        }

        var userEntity = new UserEntity();
        BeanUtils.copyProperties(userModel, userEntity);
        userEntity.setUserStatus(UserStatus.ACTIVE);
        userEntity.setUserType(UserType.STUDENT);
        userService.save(userEntity);

        return ResponseEntity.status(HttpStatus.CREATED).body(userEntity);

    }
}