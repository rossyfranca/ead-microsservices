package com.ead.authuser.controllers.util;

import com.ead.authuser.controllers.UserController;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.TemplateVariable;
import org.springframework.hateoas.TemplateVariables;
import org.springframework.hateoas.UriTemplate;
import org.springframework.stereotype.Component;

import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class LinksFactory {

    private static final TemplateVariables TEMPLATE_VARIABLES = new TemplateVariables(
            new TemplateVariable("page", TemplateVariable.VariableType.REQUEST_PARAM),
            new TemplateVariable("size", TemplateVariable.VariableType.REQUEST_PARAM),
            new TemplateVariable("sort", TemplateVariable.VariableType.REQUEST_PARAM),
            new TemplateVariable("direction", TemplateVariable.VariableType.REQUEST_PARAM));

    public Link linkToUsers() {
        var templateVariables = TEMPLATE_VARIABLES.concat(buildFilterTemplateVariables());

        var uriTemplate = UriTemplate.of(getUrl(), templateVariables);

        return Link.of(uriTemplate, "Users");
    }

    private String getUrl() {
        return linkTo(UserController.class).toUri().toString();
    }

    private TemplateVariables buildFilterTemplateVariables() {
        return new TemplateVariables(new TemplateVariable("userType", TemplateVariable.VariableType.REQUEST_PARAM),
                new TemplateVariable("email", TemplateVariable.VariableType.REQUEST_PARAM),
                new TemplateVariable("courseId", TemplateVariable.VariableType.REQUEST_PARAM),
                new TemplateVariable("userStatus", TemplateVariable.VariableType.REQUEST_PARAM));
    }

    public String buildUriLocation(UUID userId) {
        return linkTo(methodOn(UserController.class).getOneUser(userId)).toUri().toString();
    }

}
