package com.ead.authuser.controllers.view;

public interface UserEntryView {

    interface RegisterUser {}
    interface UpdateUser {}
    interface UpdatePassword {}
    interface UpdateImage {}
    interface FilterUser {}
}
